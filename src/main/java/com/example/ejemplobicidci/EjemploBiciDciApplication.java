package com.example.ejemplobicidci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjemploBiciDciApplication {

    public static void main(String[] args) {
        SpringApplication.run(EjemploBiciDciApplication.class, args);
    }

}
