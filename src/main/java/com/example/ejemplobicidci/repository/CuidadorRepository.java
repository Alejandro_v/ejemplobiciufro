package com.example.ejemplobicidci.repository;

import com.example.ejemplobicidci.model.Cuidador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CuidadorRepository extends JpaRepository<Cuidador, Integer> {

}
