package com.example.ejemplobicidci.controller;


import com.example.ejemplobicidci.model.Cuidador;
import com.example.ejemplobicidci.services.CuidadorService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("cuidador")
@RequiredArgsConstructor
public class CuidadorController {



     @Autowired
     private CuidadorService cuidadorService;


   @PostMapping("add")
  public Cuidador crearCuidador(@RequestBody Cuidador nuevoCuidador){

   return cuidadorService.agregarCuidador(nuevoCuidador);
  }


  @GetMapping("getAll")
  public List<Cuidador> obtenerCuidadores(){
       return cuidadorService.obtenerCuidadores();
  }

}
