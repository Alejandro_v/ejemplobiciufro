package com.example.ejemplobicidci.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Cuidador")
public class Cuidador {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idCuidador;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "horarioEntrada")
    private String horarioEntrada;

    @Column(name = "horaioSalida")
    private String horarioSalida;

    @Column(name="lugar")
    private String lugar;

}
